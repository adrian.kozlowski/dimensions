﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dimensions {
    public partial class Form1 : Form {

        Decimal countValuablePixels;

        public Form1() {
            InitializeComponent();
        }

        private void openDialog_Click(object sender, EventArgs e) {
            var file = openFileDialog1.ShowDialog();
            if (file == DialogResult.OK) {
                imagePath.Text = openFileDialog1.FileName;
                pictureBox.Image = Image.FromFile(imagePath.Text);
            }
            
            
        }

        private void high_ValueChanged() {
            var multiply = pictureBox.Image.Width * pictureBox.Image.Height;
            var realMyltiply = width.Value * high.Value;
            Bitmap myBitmap = new Bitmap(imagePath.Text);
            for (int x = 0; x < myBitmap.Width; x++) {
                for (int y = 0; y < myBitmap.Height; y++) {
                    Color pixelColor = myBitmap.GetPixel(x, y);
                    if(pixelColor.G<30 && pixelColor.R<30 && pixelColor.B<30) {
                        countValuablePixels++;
#if DEBUG
                        Console.WriteLine(countValuablePixels);
#endif
                    }                                      
                }
            }
            var ratio = countValuablePixels/multiply;
            result.Text =(realMyltiply * ratio).ToString();
            countValuablePixels = 0;
        }

        private void button1_Click(object sender, EventArgs e) {
            high_ValueChanged();
        }
    }
}
